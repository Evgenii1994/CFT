This project is an implementation of sorting algorithm.

You can use already builded application or build it by yourself.
 
If you want to build it by yourself:
   1. First of all you need to clone the folders on your computer (you will get CFT folder, 
      where you should delete build folder in this case)

     Then, use your console:
   2. cd CFT
   3. mkdir build
   4. cd build
   5. cmake ..
   6. cmake --build .

Now you can use it.
   1. Place input file into the build directory
   2. Depending on the input file you can use the following commands:

	./sorting input.txt output.txt -i/-s -a/-d

	Where:

	iput.txt is the name of input file

	output.txt is the name of output file

	-i for sorting integer values

	-s for sorting string values

	-a is ascending sorting

	-d is descending sorting

	You can also use ./sorting -h   or   ./sorting --help   commands for this purpose.
