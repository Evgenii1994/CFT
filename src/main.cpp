#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <boost/program_options.hpp>
#include "parser.cpp"

namespace po = boost::program_options;

using std::cerr;
using std::endl;
using std::cout;
using std::vector;
using std::string;
using std::getline;
using std::ifstream;
using std::ofstream;
using std::stoi;

template <typename T>
void swap(T& first, T& second) {
  int value = first;
  first = second;
  second = value;
}

template <typename T>
void printSequence(const vector<T>& sequence, ofstream& output, const bool status) {
  if (status == 1) {
    for (int i = 0; i < sequence.size(); ++i) {
      output << sequence[i] << " ";
    }
  } else {
    for (int i = sequence.size() - 1; i >= 0; --i) {
      output << sequence[i] << " ";
    }
  }
  output.close();
}

bool checkStreams(const ifstream& ifs, const ofstream& ofs) {
  if (!ifs && !ofs) {
    cerr << "Cannot open input and output files" << endl;
    return 1;
  }

  if (!ifs) {
    cerr << "Cannot open input file" << endl;
    return 1;
  }

  if (!ofs) {
    cerr << "Cannot open output file" << endl;
    return 1;
  }
  return 0;
}

void intSort(ifstream& input, ofstream& output, const bool status) {
  vector<int> sequence;
  string str;

  try {
    while (getline(input, str)) {
      sequence.push_back(stoi(str));
    }
  } catch (std::invalid_argument& e) {
    cerr << "Values cannot be converted to int" << endl;
  } catch (std::out_of_range& e) {
    cerr << "The value is out of int range" << endl;
  }


  for (int i = 1; i < sequence.size(); ++i) {
    int j = i - 1;
    while (j >= 0 && sequence[j] > sequence[j + 1]) {
      swap(sequence[j], sequence[j + 1]);
      --j;
    }
  }
  printSequence(sequence, output, status);
}

void strSort(ifstream& input, ofstream& output, const bool status) {
  vector<string> sequence;
  string str;

  while (getline(input, str)) {
    sequence.push_back(str);
  }

  for (int i = 0; i < sequence.size(); ++i) {
    int j = i - 1;
    while (j >= 0 && strcmp(sequence[j].c_str(), sequence[j + 1].c_str()) > 0) {
      swap(sequence[j], sequence[j + 1]);
      --j;
    }
  }
  printSequence(sequence, output, status);
}

ifstream createInputStream(string& fileName) {
  const char *cstr_1 = fileName.c_str();
  ifstream ifs(cstr_1);
  return ifs;
}

ofstream createOutputStream(string& fileName) {
  const char *cstr_1 = fileName.c_str();
  ofstream ofs(cstr_1);
  return ofs;
}

int main(int argc, char** argv) {
  Parser obj(argc, argv);

  if (obj.getVariablesMap().count("help")) {
    cout << endl << obj.getDescription() << endl;
    return 0;
  }

  string input = obj.getInputFileName();
  string output = obj.getOutputFileName();

  if (obj.getVariablesMap().count("int")) {
    if(obj.getVariablesMap().count("ascending")) {
      ifstream ifs = createInputStream(input);
      ofstream ofs = createOutputStream(output);
      checkStreams(ifs, ofs);
      intSort(ifs, ofs, 1);
      return 0;
    } else if(obj.getVariablesMap().count("descending")) {
      ifstream ifs = createInputStream(input);
      ofstream ofs = createOutputStream(output);
      checkStreams(ifs, ofs);
      intSort(ifs, ofs, 0);
    } else {
      cerr << "You should choose parameters <-a> or <-d>" << endl;
      return 1;
    }
  } else if (obj.getVariablesMap().count("string")) {
    if(obj.getVariablesMap().count("ascending")) {
      ifstream ifs = createInputStream(input);
      ofstream ofs = createOutputStream(output);
      checkStreams(ifs, ofs);
      strSort(ifs, ofs, 1);
      return 0;
    } else if(obj.getVariablesMap().count("descending")) {
      ifstream ifs = createInputStream(input);
      ofstream ofs = createOutputStream(output);
      checkStreams(ifs, ofs);
      strSort(ifs, ofs, 0);
    } else {
      cerr << "You should choose parameters <-a> or <-d>" << endl;
      return 1;
    }
  } else {
    cerr << "You should choose parameters <-i> or <-s> and <-a> or <-d>" << endl;
    return 1;
  }
  return 0;
}
