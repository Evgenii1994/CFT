#include <boost/program_options.hpp>

class Parser {
  std::string inputFileName;
  std::string outputFileName;
  boost::program_options::options_description desc;
  boost::program_options::variables_map vm;
public:
  const std::string& getInputFileName();
  const std::string& getOutputFileName();
  const boost::program_options::options_description& getDescription();
  const boost::program_options::variables_map& getVariablesMap();
  Parser(int argc, char** argv);
};
