#include <boost/program_options.hpp>
#include "parser.h"
#include <iostream>

namespace po = boost::program_options;

using std::endl;

const std::string& Parser::getInputFileName() {
  return inputFileName;
}

const std::string& Parser::getOutputFileName() {
  return outputFileName;
}

const boost::program_options::options_description& Parser::getDescription() {
  return desc;
}

const boost::program_options::variables_map& Parser::getVariablesMap() {
  return vm;
}

Parser::Parser(int argc, char** argv) {
  desc.add_options()
    ("help,h", "show help")
    ("int,i", "integer values")
    ("string,s", "string values")
    ("ascending,a", "ascending sorting")
    ("descending,d", "descending sorting")
    ;

    po::positional_options_description p;
    p.add("input-file", 1);
    p.add("output-file", 2);

    po::options_description hidden("Hidden options");
    hidden.add_options()
        ("input-file", po::value(&inputFileName), "input file")
        ("output-file", po::value(&outputFileName), "output file")
    ;

    po::options_description cmdline_options;
    cmdline_options.add(desc).add(hidden);

    try {
      po::store(po::command_line_parser(argc, argv).options(cmdline_options).positional(p).run(), vm);
      po::notify(vm);
    } catch(po::error& e) {
      std::cout << e.what() << endl;
    }
}
